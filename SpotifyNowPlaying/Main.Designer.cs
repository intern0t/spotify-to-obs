﻿namespace SpotifyNowPlaying
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.spotifyLogoFromIconFinder = new System.Windows.Forms.PictureBox();
            this.lnkDeveloper = new System.Windows.Forms.LinkLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.captureIntervalTrack = new System.Windows.Forms.TrackBar();
            this.captureSeconds = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.spotifyLogoFromIconFinder)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.captureIntervalTrack)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(17, 52);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(127, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start Capturing";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(150, 52);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(127, 23);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop Capturing";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // spotifyLogoFromIconFinder
            // 
            this.spotifyLogoFromIconFinder.Image = ((System.Drawing.Image)(resources.GetObject("spotifyLogoFromIconFinder.Image")));
            this.spotifyLogoFromIconFinder.Location = new System.Drawing.Point(90, 43);
            this.spotifyLogoFromIconFinder.Name = "spotifyLogoFromIconFinder";
            this.spotifyLogoFromIconFinder.Size = new System.Drawing.Size(150, 150);
            this.spotifyLogoFromIconFinder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.spotifyLogoFromIconFinder.TabIndex = 2;
            this.spotifyLogoFromIconFinder.TabStop = false;
            // 
            // lnkDeveloper
            // 
            this.lnkDeveloper.AutoSize = true;
            this.lnkDeveloper.Location = new System.Drawing.Point(60, 506);
            this.lnkDeveloper.Name = "lnkDeveloper";
            this.lnkDeveloper.Size = new System.Drawing.Size(197, 13);
            this.lnkDeveloper.TabIndex = 3;
            this.lnkDeveloper.TabStop = true;
            this.lnkDeveloper.Text = "Copyright © 2017, Prashant Shrestha";
            this.lnkDeveloper.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkDeveloper_LinkClicked);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.captureSeconds);
            this.groupBox1.Controls.Add(this.captureIntervalTrack);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnStart);
            this.groupBox1.Controls.Add(this.btnStop);
            this.groupBox1.Location = new System.Drawing.Point(12, 236);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(293, 87);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Controls";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(12, 329);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(293, 164);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Note";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(277, 130);
            this.label1.TabIndex = 0;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Capture Interval";
            // 
            // captureIntervalTrack
            // 
            this.captureIntervalTrack.AutoSize = false;
            this.captureIntervalTrack.LargeChange = 2;
            this.captureIntervalTrack.Location = new System.Drawing.Point(100, 17);
            this.captureIntervalTrack.Maximum = 30;
            this.captureIntervalTrack.Minimum = 1;
            this.captureIntervalTrack.Name = "captureIntervalTrack";
            this.captureIntervalTrack.Size = new System.Drawing.Size(128, 23);
            this.captureIntervalTrack.TabIndex = 3;
            this.captureIntervalTrack.Value = 1;
            this.captureIntervalTrack.Scroll += new System.EventHandler(this.captureIntervalTrack_Scroll);
            // 
            // captureSeconds
            // 
            this.captureSeconds.AutoSize = true;
            this.captureSeconds.Location = new System.Drawing.Point(232, 21);
            this.captureSeconds.Name = "captureSeconds";
            this.captureSeconds.Size = new System.Drawing.Size(46, 13);
            this.captureSeconds.TabIndex = 4;
            this.captureSeconds.Text = "X sec(s).";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 534);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lnkDeveloper);
            this.Controls.Add(this.spotifyLogoFromIconFinder);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Text = "Spotify Now Playing - OBS";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.spotifyLogoFromIconFinder)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.captureIntervalTrack)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.PictureBox spotifyLogoFromIconFinder;
        private System.Windows.Forms.LinkLabel lnkDeveloper;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label captureSeconds;
        private System.Windows.Forms.TrackBar captureIntervalTrack;
        private System.Windows.Forms.Label label2;
    }
}

