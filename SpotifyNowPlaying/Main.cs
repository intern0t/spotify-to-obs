﻿using SpotifyNowPlaying.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace SpotifyNowPlaying
{
    public partial class Main : Form
    {
        private static int spotifyProcessID = 0;
        private static String spotifyNowPlayingTrack = string.Empty;
        private static System.Windows.Forms.Timer T = new System.Windows.Forms.Timer();
        private static int captureInterval = 2;

        public Main()
        {
            InitializeComponent();
            T.Tick += T_Tick;
        }

        private void T_Tick(object sender, EventArgs e)
        {
            new Thread(new ThreadStart(UpdateSong)).Start();
        }

        public string GetSpotifyProcesses()
        {
            string _spotifyHandle = string.Empty;
            Process[] spotifyProcesses = Process.GetProcessesByName("spotify");

            foreach (Process P in spotifyProcesses)
            {
                if (!string.IsNullOrEmpty(P.MainWindowTitle))
                {
                    spotifyProcessID = P.Id;
                    // Change the Picture
                    spotifyLogoFromIconFinder.Image = Resources.spotOn;
                    break;
                }
                else
                {
                    spotifyProcessID = 0;
                    spotifyLogoFromIconFinder.Image = Resources.spotOn;
                }
            }

            return _spotifyHandle;
        }

        public string GetSpotifyTrack(int _procTitle)
        {
            string spotifyTrack = Process.GetProcessById(_procTitle).MainWindowTitle;
            spotifyNowPlayingTrack = spotifyTrack;

            return spotifyTrack.ToLower().Contains("spotify") ? 
                "Currently not playing any songs!" : 
                "Spotify (Now Playing) : " + spotifyTrack;
        }

        public void UpdateSong()
        {
            try
            {
                // Check if current playing is the same as old one we have.
                string currentlyPlaying = GetSpotifyTrack(spotifyProcessID);

                if (!currentlyPlaying.Equals(spotifyNowPlayingTrack))
                {
                    // Check if our file exists.
                    if (File.Exists("SpotifyNowPlaying"))
                    {
                        // If it already exists, write the title & be done!
                        File.WriteAllText("SpotifyNowPlaying", currentlyPlaying);
                        Console.WriteLine(currentlyPlaying);
                    }
                    else
                    {
                        // Doesn't exist, create it.
                        File.Create("SpotifyNowPlaying");
                        // Recursive Function
                        UpdateSong();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please open Spotify first and then this tool second!");
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            GetSpotifyProcesses();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (spotifyProcessID != 0)
            {
                T.Interval = captureInterval * 1000;
                T.Enabled = true;
            }
            else
            {
                T.Enabled = false;
                MessageBox.Show("Please run Spotify first and launch this tool, thanks!");
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            T.Enabled = false;
        }

        private void captureIntervalTrack_Scroll(object sender, EventArgs e)
        {
            int captureTrackValue = captureIntervalTrack.Value;
            captureSeconds.Text = string.Format("{0} sec(s).", captureTrackValue);
            T.Interval = captureTrackValue * 1000;
        }

        private void lnkDeveloper_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start("https://prashant.me/");
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}